package com.epam.Epam_Vistor_Analysis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpamVistorAnalysisApplication {

	public static void main(String[] args) {
		SpringApplication.run(EpamVistorAnalysisApplication.class, args);
	}

}
